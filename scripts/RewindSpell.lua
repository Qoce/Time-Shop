local clientActionBuffer = require "necro.client.ClientActionBuffer"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local entitySelector = require "system.events.EntitySelector"
local field = components.field
local customActions = require "necro.game.data.CustomActions"
local customEntities = require "necro.game.data.CustomEntities"
local entities = require "system.game.Entities"
local event = require "necro.event.Event"
local inventory = require "necro.game.item.Inventory"
local map = require "necro.game.object.Map"
local object = require "necro.game.object.Object"
local player = require "necro.game.character.Player"
local rewind = require "Rewind.Rewind"
local rollback = require "necro.client.Rollback"
local segment = require "necro.game.tile.Segment"
local snapshot = require "necro.game.system.Snapshot"
local spell = require "necro.game.spell.Spell"
local tile = require "necro.game.tile.Tile"
local turn = require "necro.cycles.Turn"
local utils = require "system.utils.Utilities"


components.register{
    spellcastRewindToTarget = {},
    spellcastRewind = {},
    spellcastRewindScroll = {},
}

commonSpell.registerSpell("spellcastRewindToTarget", {
    TimeShop_spellcastRewindToTarget = {}
})

commonSpell.registerSpell("spellcastRewind", {
    TimeShop_spellcastRewind = {}
})

commonSpell.registerSpell("spellcastRewindScroll", {
    TimeShop_spellcastRewindScroll = {}
})



customEntities.extend {
    name = "ScrollRewind",
    template = customEntities.template.item(),

    data = {
        flyaway = "Rewind Scroll",
        hint = "Rewrite the Stars",
        slot = "action"
    },
    components = {
        itemCastOnUse = {
            spell  = "TimeShop_spellcastRewindScroll"
        },
        sprite = {
            texture = "mods/TimeShop/gfx/rewindScroll.png"
        },
    }
}

customEntities.extend {
    name = "SpellRewind",
    template = customEntities.template.item(),

    data = {
        flyaway = "Rewind Spell",
        hint = "Rewrite the Stars",
        slot = "spell"
    },
    components = {
        itemCastOnUse = {
            spell  = "TimeShop_spellcastRewind"
        },
        sprite = {
            texture = "mods/TimeShop/gfx/rewindSpell.png"
        },
        spellCooldownKills = {
            cooldown = 20
        },
        spellBloodMagic = {
            damage = 2
        }
    }
}


 
event.spellcast.add("spellcastRewindToTarget", {order = "teleport", sequence = 1, filter = "TimeShop_spellcastRewindToTarget"}, function(ev)
    local minTurn = turn.getActiveTurnID()
    local bestTarget = nil
    if tile.get(ev.x, ev.y) ~= tile.EMPTY then
        local targets = map.getAll(ev.x, ev.y)
        for _,t in pairs(targets) do
            local target = entities.getEntityByID(t)
            if target and target:hasComponent("TimeShop_canBeRewinded") then
                minTurn = math.min(minTurn, target.TimeShop_canBeRewinded.destinationTurnID)
                bestTarget = target
            end
        end
    end
    if bestTarget then
        rewind.rewindToTurnID(bestTarget, minTurn, {
            caster = ev.caster.id
        })
    end
end)

event.spellcast.add("spellcastRewind", {order = "teleport", sequence = 1, filter = "TimeShop_spellcastRewind"}, function(ev)   
    local castItem
    for _,item in ipairs(inventory.getItemsInSlot(ev.caster, "spell")) do
        if item.name == "TimeShop_SpellRewind" then
            castItem = item
        end
    end
    rewind.rewindNumberOfTurns(ev.caster, 9, {
        caster = ev.caster.id,
        castItem = castItem.id,
        bloodCasted = castItem.spellCooldownKills.remainingKills > 0
    })
end)

event.spellcast.add("spellcastRewindScroll", {order = "teleport", sequence = 1, filter = "TimeShop_spellcastRewindScroll"}, function(ev)   
    local caster = ev.caster
    local castItem
    for _,item in ipairs(inventory.getItemsInSlot(caster, "action")) do
        if item.name == "TimeShop_ScrollRewind" then
            castItem = item
        end
    end
    rewind.rewindNumberOfTurns(ev.caster, 9, {
        castItem = castItem.id
    })
end)

event.postRewind.add("closeSecretShop", "postRewind", function(ev)
    local caster
    if ev and ev.caster then
        caster = entities.getEntityByID(ev.caster)
    end
    if caster and caster.name == "TimeShop_TimeLord" then
        for rune in entities.entitiesWithComponents {"TimeShop_canDeactivate"} do
            rune.TimeShop_canDeactivate.locked = true
            if segment.getSegmentIDAt(rune.position.x, rune.position.y) == 1 then
                object.spawn("TimeShop_ScrollRewind", rune.position.x, rune.position.y)
            end
        end
    end
end)

event.postRewind.add("eraseScroll", "postRewind", function(ev)
    local castItem
    if ev and ev.castItem then
        castItem = ev.castItem
    end
    if castItem then
        local item = entities.getEntityByID(castItem)
        if item and item.name == "TimeShop_ScrollRewind" then
            inventory.destroy(entities.getEntityByID(castItem))
        elseif item and item.name == "TimeShop_SpellRewind" then
            item.spellCooldownKills.remainingKills = item.spellCooldownKills.cooldown 
            if ev.bloodCasted then
                local caster = entities.getEntityByID(ev.caster)
                spell.payBloodCost(caster, item.spellBloodMagic.damage)
            end
        end
    end
end)