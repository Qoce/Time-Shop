local attack = require "necro.game.character.Attack"
local camera = require "necro.render.Camera"
local components = require "necro.game.data.Components"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local ecs = require "system.game.Entities"
local enum = require "system.utils.Enum"
local event = require "necro.event.Event"
local field = components.field
local gfx = require "system.game.Graphics"
local itemGeneration = require "necro.game.item.ItemGeneration"
local levelUtils = require "necro.game.level.LevelUtils"
local object = require "necro.game.object.Object"
local perspective = require "necro.game.system.Perspective"
local playerSystem = require "necro.game.character.Player"
local render = require "necro.render.Render"
local segment = require "necro.game.tile.Segment"
local settings = require "necro.config.Settings"
local speedrunTimer = require "necro.client.SpeedrunTimer"
local turn = require "necro.cycles.Turn"
local ui = require "necro.render.UI"

local hcss, customSecretShop = pcall(require, "CustomSecretShop.CustomSecretShop")

if not hcss then
    error("Need to have CustomSecretShop mod active to use TimeShop")
end

local debug = false

local hasTimeLimit = enum.sequence{
    ON = 1,
    OFF = 2,
}

HasTimeLimit = settings.shared.enum {
    name = "Rune Time Limit",
    defualt = 1,
    enum = hasTimeLimit
}

TimeLimitPerFloor = settings.shared.number {
    name = "Seconds Per Floor",
    default = 30,
    minimum = 10,
    step = 2
}

ExtraPlayerMultiplier = settings.shared.number {
    name = "Extra Player Multiplier",
    default = 1.2,
    minimum = 0.5,
    step = 0.05
}



components.register{
    canDeactivate = {
        field.int32("cooldown", 0),
        field.bool("locked", false)
    },
    canPayTime = {
        field.int32("cooldown", 0)
    },
    priceTagCostBeats = {
        field.int32("cost", 1),
        field.entityID("rune"),
    },
    canSpawnInTimeShop = {
        field.int32("cost", 0)
    },
    canBeRewinded = {
        field.int32("destinationTurnID", 0)
    },
}

customSecretShop.reset() 
local shopID = customSecretShop.addSecretShop("TimeShop", "TimeShopRune", 5, 7, "mods/TimeShop/gfx/timeShopTravelRune.png", 1, {TimeShop_canDeactivate = {cooldown = 0}})
--local shopID =  customSecretShop.addCustomShop("TimeShop", "TimeShopRune", 5, 7, 
--"mods/TimeShop/gfx/timeShopTravelRune.png", customSecretShop.isAnyNonBossFloor,  
--customSecretShop.openTileInStartingRoom, false, {TimeShop_canDeactivate = {}})
--local shopID = customSecretShop.addDemoRoom("TimeShop", "mods/TimeShop/gfx/timeShopTravelRune.png", {})

customEntities.register {
	name = "PriceTagBeats",
	gameObject = {},
	position = {},
	priceTag = {},
--	priceTagShopkeeperProximity = {},
	TimeShop_priceTagCostBeats = {},
    priceTagLabelInlineImage = {
        texture = "mods/TimeShop/gfx/mini_beat_bar.png",
        costMultiplier = 1,
    },
	priceTagLabel = { offsetX = -2 },
	priceTagUnaffordableFlyaway = {},
	soundPriceTagPurchaseSuccess = {},
	soundPriceTagPurchaseFail = {},
	friendlyName = {
		name = "The Doctor?!"
	},
}

customEntities.extend {
	name = "TimeLord",
	template = customEntities.template.enemy("transmogifier"),
    components = {
        initialEquipment = {items = {"TimeShop_SpellRewind"}},
        sprite = {
            texture = "mods/TimeShop/gfx/timelord.png"
        },
        castOnHit = { 
            spell = "TimeShop_spellcastRewindToTarget",
            castOnAttackerPosition = true,
        },
        positionalSprite = {},
        shopkeeper = {},
        homeArea = {},
        --minimapStaticPixel = {color = minimapTheme.Color.NPC},
        inventory = {},
        inventoryDropItemsOnDeath = {},
        inventoryMarkSeen = {},
        HealthShop_healthShopkeeper = {},
        provokable = {},
        secretShopLabel = {text = "Spend time here to get items?", offsetY = 15},
        normalAnimation = {frames = {1,2,3,4}},
        attackable = {flags = attack.Flag.PROVOKE},
        TimeShop_rewindTarget = {},
    }
}


event.entitySchemaLoadPlayer.add("addPriceTagBeats", {order = "overrides", sequence = 1}, function(ev)
    if not ev.entities[1].rhythmIgnored or debug then
        ev.entities[1].TimeShop_canPayTime = {
            cooldown = 0
        }
    end
    ev.entities[1].TimeShop_canBeRewinded = {
        destinationTurnID = 0
    }
end)

local itemPool = {

{"RingPeace",1,25},
{"RingPain",1,25},
{"RingCourage",1,25},

{"FeetBootsLeaping",1,50},
{"FeetBootsLunging",1,75},
{"FeetBootsPain",2,15},

{"ShovelCourage",1,40},
{"ShovelStrength",1,15},

{"WeaponTitaniumAxe",1,40},
{"WeaponObsidianAxe",1,40},
{"WeaponTitaniumRapier",1,20},
{"WeaponObsidianRapier",1,20},
{"WeaponTitaniumCat",1,20},
{"WeaponObsidianCat",1,20},
{"WeaponTitaniumHarp",1,10},
{"WeaponObsidianHarp",1,10},
{"WeaponTitaniumStaff",1,10},
{"WeaponObsidianStaff",1,10},
{"WeaponDaggerElectric",2,20},

{"HeadBlastHelm",1,20},
{"HeadMinersCap",1,20},
{"HeadMonacle",1,10},

{"TorchStrength",1,15},
{"TorchForesight",1,10},
{"TorchFalls",1,15},

{"ArmourHeavyplate",1,20},
{"ArmourHeavyglass",1,15},
{"ArmourPlatemail",1,15},

{"SpellFreezeEnemies",1,25},
{"SpellShield",1,25},
{"SpellEarth",1,15},
{"SpellPulse",1,15},
{"SpellHeal",1,15},

{"CharmStrength",1,20},
{"CharmBomb",1,15},
{"CharmGrenade",1,20},
{"CharmProtection",1,10},

{"MiscHeartContainer2",1,20},
{"MiscHeartContainerEmpty2",1,10},
{"MiscCompass",1,15},
{"MiscMap",1,25}}

local priceMap = {}
for x,t in ipairs(itemPool) do
    priceMap[t[1]:upper()]=t[3]
end


--Adds prices to items that can appear in time shop
event.entitySchemaLoadItem.add("addBeatPrices", {order = "uncertainty", sequence = 1}, function (ev)
    if priceMap[ev.data.name:gsub('%_', ''):upper()] then
        local price = priceMap[ev.data.name:gsub('%_', ''):upper()]
        ev.entity.TimeShop_canSpawnInTimeShop = {
            cost = price
        }
    end
end)

local function selectItem(good)
    if good then
        return itemGeneration.unweightedChoice(0,nil,"TimeShop_canSpawnInTimeShop")
    else
        return itemGeneration.weightedChoice(0,"chest",0)
    end
end

local function placeItem(bounds, level, dx, dy, good)
    local cx = math.ceil(bounds[1] + bounds[3] / 2 - 1)
    local cy = math.ceil(bounds[2] + bounds[4] / 2 - 1)
    local itemID = selectItem(good)
    local itemEntity = levelUtils.makeEntity(itemID, cx + dx, cy + dy)
    itemEntity.price = levelUtils.makeEntity("TimeShop_PriceTagBeats")
    local depth = currentLevel.getDepth()
    local multiplier = 1
    if depth == 1 then
        multiplier = 3
    elseif depth == 2 then
        multiplier = 2
    elseif depth == 3 then
        multiplier = 1.5
    end
    local cost
    if ecs.getEntityPrototype(itemID):hasComponent("TimeShop_canSpawnInTimeShop") then
        levelUtils.addEntityAttribute(itemEntity.price, "TimeShop_priceTagCostBeats", "cost", math.ceil(ecs.getEntityPrototype(itemID).TimeShop_canSpawnInTimeShop.cost * multiplier))
    else
        levelUtils.addEntityAttribute(itemEntity.price, "TimeShop_priceTagCostBeats", "cost", math.ceil(math.sqrt(ecs.getEntityPrototype(itemID).itemPrice.coins) * multiplier))
    end


    level.entities[#level.entities + 1] = itemEntity
    level.entities[#level.entities + 1] = itemEntity.price
end

local function placeItems(bounds, level)
    for dx = -1,1 do
        placeItem(bounds, level, dx, 0, dx == 0)
    end
end

event.levelLoad.add("fillTimeShop", {order = "levelBounds", sequence = 2}, function(ev)
    local bounds = customSecretShop.getBounds(shopID)
    if bounds then
        placeItems(bounds,ev)
        local cx = bounds[1] + math.floor(bounds[3] * 1/2)
        local cy = bounds[2] + math.floor(bounds[4] * 1/2)
        object.spawn("TimeShop_TimeLord", cx, cy - 2)
    end
end)

local function renderNumber(number, x, y)
	return ui.drawText {
		font = ui.Font.DIGITS,
		x = x+ 11,
		y = y,
		alignX = 0.5,
		text = tostring(number),
		size = ui.Font.DIGITS.size,
        buffer = render.Buffer.TEXT_LABEL
	}
end

event.priceTagCheck.add("renderTimeShopLabels", 
{order = "health", sequence = 1,filter = {"TimeShop_priceTagCostBeats"}}, 
function (ev)
    ev.effectiveCost = ev.price.TimeShop_priceTagCostBeats.cost
    ev.affordable = ev.buyer.TimeShop_canPayTime
end)

--Add to cooldown of all time shop travel runes on the map everytime an item with a beat price tag is purchased
event.priceTagPay.add("payBeats", {order = "health", filter = {"TimeShop_priceTagCostBeats", "priceTag"}}, function(ev)
    --dbg(ev)
    ev.buyer.TimeShop_canPayTime.cooldown = 
    ev.buyer.TimeShop_canPayTime.cooldown + 
    ev.price.TimeShop_priceTagCostBeats.cost
end)

event.objectMoveResult.add("refreshRune", {order = "ai", sequence = 1, filter = {"controllable"}}, function(ev)
    if ev.entity.controllable.playerID ~= 0 then
        if ev.entity.TimeShop_canPayTime then
            ev.entity.TimeShop_canPayTime.cooldown = 
            math.max(ev.entity.TimeShop_canPayTime.cooldown - 1, 0)
        end
    end
end)

event.updateVisuals.add("runeVisuals", {order = "trapVisuals", sequence = 1}, function ()
    local cooldown = 0
    for player in ecs.entitiesWithComponents {"TimeShop_canPayTime"} do
        cooldown = math.max(cooldown, player.TimeShop_canPayTime.cooldown)
    end
    local shouldLock = false
    if HasTimeLimit == hasTimeLimit.ON then
        if currentLevel.getDepth() then
            local maxTime = TimeLimitPerFloor * (4 * (currentLevel.getDepth() - 1) + currentLevel.getFloor())
            for _,__ in ipairs(playerSystem.getPlayerEntities()) do
                maxTime = maxTime * ExtraPlayerMultiplier
            end
            maxTime = maxTime / ExtraPlayerMultiplier --avoids OBOE. Don't multiply first player
            if speedrunTimer.getTime() > maxTime then
                shouldLock = true
            end
        end
    end
	for trap in ecs.entitiesWithComponents {"TimeShop_canDeactivate"} do
        if shouldLock and segment.getSegmentIDAt(trap.position.x, trap.position.y) == 1 then
            trap.TimeShop_canDeactivate.locked = true
        end
        trap.TimeShop_canDeactivate.cooldown = cooldown
		if trap.TimeShop_canDeactivate.locked then
            trap.spriteSheet.frameX = 3
        elseif cooldown > 0 then
            trap.spriteSheet.frameX = 1
        else
            trap.spriteSheet.frameX = 2
        end
	end
end)

--Display cooldown on the time shop travel rune
event.render.add("TimeRuneTimer", {order = "itemLabels"}, function(ev)
    for rune in ecs.entitiesWithComponents {"TimeShop_canDeactivate"} do
        if rune.TimeShop_canDeactivate.cooldown > 0 and not rune.TimeShop_canDeactivate.locked then
            renderNumber(rune.TimeShop_canDeactivate.cooldown, rune.sprite.x + 1, rune.sprite.y-3)
        end
    end
end)

--Don't auto re-arm the time shop runes
--I tried not doing this with an override
--But it gets wonky in multiplayer due to victimMove so the only solution I know of right now is to just override
--Removing VictimMove component from the rune would also work but I didn't want to make a special case in CustomSecretShop
event.objectMove.override("reArmTraps", {sequence = 1}, function(func, ev)
    if not ev.trap:hasComponent("TimeShop_canDeactivate") then
        func(ev)
    end
end)

function runIfActive(ev, func)
    if ev.trap:hasComponent("TimeShop_canDeactivate") then
        if ev.trap.TimeShop_canDeactivate.cooldown == 0 and 
            not ev.trap.TimeShop_canDeactivate.locked then
            if segment.getSegmentIDAt(ev.trap.position.x, ev.trap.position.y) == 1 then
                ev.victim.TimeShop_canBeRewinded.destinationTurnID = turn.getActiveTurnID()
            end
            func(ev)
        end
    else
        func(ev)
    end
end

event.trapTrigger.override("travel", {sequence = 1}, function(func,ev)
    runIfActive(ev,func)
end)

event.trapTrigger.override("trapVocalize", {sequence = 1}, function(func,ev)
    runIfActive(ev,func)
end)

event.trapTrigger.override("playSuccessSound", {sequence = 1}, function(func,ev)
    runIfActive(ev,func)
end)