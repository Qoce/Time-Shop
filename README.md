Dependency: Requires CustomSecretShop (1.3)
Dependency: Requires Rewind (1.1)

This mod adds a time shop, a new secret shop that spawns with three items. These items have a price in beats, represented by a green beat bar sprite. When you purchase an item, the travel rune to and from this shop will deactivate for the number of beats the item cost. If you have means of teleporting out of the shop, you are not prevented from doing so. "Beats" refers to turns of the player rather than beats of music, so if you have a heart transplant you can make the timer go down quicker.

The middle item comes from a special custom item pool consisting only of very solid items, each with a custom hardcoded beat price.
The left and right items come from the normal chest item pool of the floor you are on (no bonus). If this happens to be an item in the item pool of the middle item, it will use the hardcoded price from this item pool. Otherwise, the price is the square root of the gold price rounded down.
To compensate for the fact that it's more benificial to get the items earlier, the time shop is more expensive on earlier floors. Price multipliers for each depth are set to (3,2,1.5,1,1). Charisma and darkness both work on this shop.

The time shop has a shopkeeper. It has one heart, but if you inflict damage to him, it will rewind the game to the beat before you entered the time shop, and then it will permanently deactivate the travel rune to enter the time shop. After doing this, it spawns one rewind scroll on the exit rune. If the shopkeeper dies by natural causes, it will not rewind time, and instead drops a rewind spell.

This mod adds the rewind spell. Casting rewind rewinds the game 10 actions earlier from the caster's perspective. It cannot rewind to a previous level.

To balance it for slowplaying, the time shop rune will deactivate after 30 seconds * the current floor. This can be disabled in settings. Additional players increase the time limit by a factor of 1.2 per player, stacking exponentially. 

The sprite for the time shop travel rune is by Snowball. The sprite for the shopkeeper is by Sipher Nil. Grimmy provided a huge amount of assistance in getting rewind to work properly.
